import os,sys
import random
import math
import json
import numpy as np
import torch
import pickle
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions.bernoulli import Bernoulli
from torch.distributions.gamma import Gamma
import itertools
from sklearn.metrics import r2_score

import rewardfunctions

torch.cuda.empty_cache()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Define the actor-critic

class Actor(nn.Module):
    def __init__(self, p=0.2, concentration=0.9, nr_of_tiers=10):
        super(Actor, self).__init__()
        self.p = nn.Parameter(torch.tensor([p for x in range(nr_of_tiers)]))
        self.concentration = nn.Parameter(torch.tensor([concentration for x in range(nr_of_tiers)]))
        self.averages = torch.tensor([
            1.22,
            2.33,
            3.57,
            5.01,
            6.86,
            9.44,
            13.46,
            20.30,
            32.48,
            54.91
        ])
        # self.rate = nn.Parameter(torch.tensor([rate]))

    def forward(self, nr_of_sessions=1000, session_length=50, nr_of_tiers=10):
        # self.p.data = torch.clamp(self.p.data, 0., 1.)
        sample_shape = (nr_of_sessions,session_length)
        b = Bernoulli(torch.sigmoid(self.p))
        b_samples = b.sample(sample_shape).squeeze()
        # print(b_samples)
        b_log_probs = b.log_prob(b_samples)
        # print(b_log_probs)
        rate = F.softplus(self.concentration) / (self.averages.to(device) / torch.sigmoid(self.p))
        gamma = Gamma(F.softplus(self.concentration), rate)
        gamma_samples = gamma.sample(sample_shape).squeeze()
        # print(gamma_samples)
        gamma_log_probs = gamma.log_prob(gamma_samples)
        # print(gamma_log_probs)
        # actions = (b_samples * gamma_samples / gamma.mean) / torch.sigmoid(self.p.data)
        actions = b_samples * gamma_samples
        log_probs = gamma_log_probs + b_log_probs
        return torch.transpose(actions,1,2), torch.transpose(log_probs,1,2)

class Critic(nn.Module):
    def __init__(self, session_length=50, nr_of_tiers=10, hidden_size=10, output_size=1):
        super(Critic, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(session_length + nr_of_tiers, session_length),
            nn.ReLU(),
            nn.Linear(session_length, session_length),
            nn.ReLU(),
            nn.Linear(session_length, session_length),
        )

    def forward(self, actions):
        return self.layers(actions)
        # x = actions - 0.95
        # p = torch.clamp(torch.pow(x,2), 0., 1.)
        # b = Bernoulli(p)
        # # rewards = torch.ones(actions.size())
        # # rewards[actions == 0] = 0
        # rewards = b.sample()
        # return rewards

def train(actor, critic, model, optim_a, optim_c, iterations=100, nr_of_sessions=1000, save_to_file=None, rewardfunction=rewardfunctions.do_nothing, tau=33.33):
    torch.cuda.empty_cache()
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    actor.train()
    critic.train()
    # print(device)
    if save_to_file != None:
        with open(save_to_file, 'w') as f:
            f.write('iteration,mean,mean_per_tier,std,full_session,pred_mean,c_loss,a_loss,p,concentration\n')

    for x in range(iterations):
        # print(x)
        out = train_iteration(actor, critic, model, optim_a, optim_c, nr_of_sessions, rewardfunction=rewardfunction, tau=tau)
        if save_to_file != None:
            with open(save_to_file, 'a') as f:
                f.write(str(x)+','+out+'\n')

def train_iteration(actor, critic, model, optim_a, optim_c, nr_of_sessions=1000, rewardfunction=rewardfunctions.do_nothing, tau=33.33):
    # Setting everything ready for training
    actor.zero_grad()
    critic.zero_grad()

    # Feeding outputs from the actor to the user model
    rewards, log_probs = actor(nr_of_sessions)
    sessions = rewards.detach().cpu().numpy()
    model_response = model.play(sessions)
    user_actions = torch.from_numpy(model_response[0]).to(device)
    user_tiers = torch.from_numpy(model_response[1]).to(device).float()
    users_per_cohort = model_response[2]
    if len(user_actions) > 0:
        session_length = torch.sum(user_actions, dim=1, dtype=torch.float).unsqueeze(1)
        session_length_per_tier = (torch.mean((session_length * user_tiers).float(), dim=0) * len(user_tiers) / torch.sum(user_tiers, dim=0)).cpu().tolist()
        rewards_matched = None
        if len(user_actions) == len(rewards):
            rewards_matched = rewards
        else:
            for nr in users_per_cohort:
                if rewards_matched == None:
                    rewards_matched = rewards[:nr][:][:]
                    continue
                rewards_matched = torch.cat((rewards_matched, rewards[:nr][:][:]),0)
        # print(session_length)

        # Training the critic with the outputs of actor and user model
        critic.zero_grad()
        critic_examples = torch.cat((torch.masked_select(rewards_matched,user_tiers.unsqueeze(2).bool()).reshape(user_actions.shape), user_tiers), 1)
        critic_targets = user_actions
        # critic_targets = session_length
        critic_outputs = critic(critic_examples)
        criterion = nn.BCEWithLogitsLoss()
        # criterion = nn.MSELoss()
        loss_c = criterion(critic_outputs, critic_targets)
        loss_c.backward()
        optim_c.step()

        # Generating new outputs from the actor to train the actor
        actor.zero_grad()
        actions, log_probs = actor(nr_of_sessions)
        actions_matched = None
        log_probs_matched = None
        if len(user_actions) == len(actions):
            actions_matched = actions
            log_probs_matched = log_probs
        else:
            for nr in users_per_cohort:
                if actions_matched == None:
                    actions_matched = actions[:nr][:][:]
                    log_probs_matched = log_probs[:nr][:][:]
                    continue
                actions_matched = torch.cat((actions_matched, actions[:nr][:][:]),0)
                log_probs_matched = torch.cat((log_probs_matched, log_probs[:nr][:][:]),0)

        actions_per_tier = torch.cat((torch.masked_select(actions_matched,user_tiers.unsqueeze(2).bool()).reshape(user_actions.shape), user_tiers), 1)
        log_probs_per_tier = torch.masked_select(log_probs_matched,user_tiers.unsqueeze(2).bool()).reshape(user_actions.shape)
        # rewards = torch.sigmoid(critic(actions))
        sess = torch.sum(torch.sigmoid(critic(actions_per_tier)), dim=1, dtype=torch.float).unsqueeze(dim=1)
        rewards = rewardfunction(sess,tau)
        loss_a = -torch.mean(torch.sum(log_probs_per_tier,dim=1).unsqueeze(dim=1) * rewards)
        loss_a.backward()
        optim_a.step()

        (counts, bins) = np.histogram(session_length.detach().cpu().numpy(), 50, range=(0,50))
        # print(f"Mean actions: {round(torch.mean(session_length).item(),2)} - Std actions: {round(torch.std(session_length).item(),2)} - Nr of 50: {counts[-1]} - Predicted average actions: {round(torch.mean(rewards).item(),2)} - C Loss: {round(loss_c.item(),4)} - A Loss: {round(loss_a.item(),4)} - P of A: {round(torch.sigmoid(a.p.data).item(),4)} - Conc.: {round(F.softplus(a.concentration.data).item(), 4)}")
        return f"{round(torch.mean(session_length).item(),4)},'{','.join(str(e) for e in session_length_per_tier)}',{round(torch.std(session_length).item(),4)},{counts[-1]},{round(torch.mean(sess).item(),4)},{round(loss_c.item(),4)}, {round(loss_a.item(),4)},'{','.join(str(e) for e in torch.sigmoid(actor.p.data).cpu().numpy().round(4).tolist())}','{','.join(str(e) for e in F.softplus(actor.concentration.data).cpu().numpy().round(4).tolist())}'"
        # return session_length.detach().cpu().numpy(), rewards.detach().cpu().numpy()
    else:
        return ",,,,,,,,"

def eval_critic(actor, critic, model, optim_a, optim_c, iterations=100, nr_of_sessions=1000, save_to_file=None, rewardfunction=rewardfunctions.do_nothing, tau=33.33):
    torch.cuda.empty_cache()
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    actor.eval()

    if save_to_file != None:
        with open(save_to_file, 'w') as f:
            f.write('iteration,mean,pred_mean,abs_mean_diff,r2,residuals,predictions\n')

    for x in range(iterations):
        # print(x)
        out = eval_critic_iteration(actor, critic, model, optim_a, optim_c, nr_of_sessions, rewardfunction=rewardfunction, tau=tau)
        if save_to_file != None:
            with open(save_to_file, 'a') as f:
                f.write(str(x)+','+out+'\n')

def eval_critic_iteration(actor, critic, model, optim_a, optim_c, nr_of_sessions=1000, rewardfunction=rewardfunctions.do_nothing, tau=33.33):

    # Setting everything ready for training
    actor.zero_grad()
    critic.zero_grad()

    # Feeding outputs from the actor to the user model
    rewards, log_probs = actor(nr_of_sessions)
    sessions = rewards.detach().cpu().numpy()
    model_response = model.play(sessions)
    user_actions = torch.from_numpy(model_response[0]).to(device)
    user_tiers = torch.from_numpy(model_response[1]).to(device).float()
    users_per_cohort = model_response[2]
    if len(user_actions) > 0:
        session_length = torch.sum(user_actions, dim=1, dtype=torch.float).unsqueeze(1)
        session_length_per_tier = (torch.mean((session_length * user_tiers).float(), dim=0) * len(user_tiers) / torch.sum(user_tiers, dim=0)).cpu().tolist()
        rewards_matched = None
        if len(user_actions) == len(rewards):
            rewards_matched = rewards
        else:
            for nr in users_per_cohort:
                if rewards_matched == None:
                    rewards_matched = rewards[:nr][:][:]
                    continue
                rewards_matched = torch.cat((rewards_matched, rewards[:nr][:][:]),0)

        # Training the critic with the outputs of actor and user model
        critic.zero_grad()
        critic_examples = torch.cat((torch.masked_select(rewards_matched,user_tiers.unsqueeze(2).bool()).reshape(user_actions.shape), user_tiers), 1)
        critic_targets = user_actions
        # critic_targets = session_length
        critic_outputs = torch.sigmoid(critic(critic_examples))
        # criterion = nn.BCEWithLogitsLoss()
        criterion = nn.MSELoss()
        loss_c = criterion(critic_outputs.double(), critic_targets.double())
        loss_c.backward()
        optim_c.step()

        # Passing the outputs of the actor through the critic again to be able to calculate the r2
        critic.zero_grad()
        sess = torch.sum(torch.sigmoid(critic(critic_examples)), dim=1, dtype=torch.float).unsqueeze(dim=1)

        mean = round(torch.mean(session_length).item(),4)
        pred_mean = round(torch.mean(sess).item(),4)
        r2 = r2_score(session_length.cpu().detach().numpy(),sess.cpu().detach().numpy())
        residuals = predictions = []
        if len(session_length) >= 100:
            residuals,predictions = zip(*random.sample(list(zip((session_length - sess).cpu().detach().tolist(), sess.cpu().detach().tolist())), 100))
        return f"{mean},{pred_mean},{abs(mean-pred_mean)},{r2},'{','.join(str(e) for l in residuals for e in l)}','{','.join(str(e) for l in predictions for e in l)}'"
    else:
        return ",,,,,"

