import torch
import torch.nn.functional as F

# Just ignore the sessions that produced outputs that are too long

def clamp(tensor, target_session_length):
    return torch.clamp(tensor, max=target_session_length)

# Linearly scale the rewards towards and from the target session length
def lin_scale(tensor, target_session_length):
    return torch.abs(tensor - target_session_length) * -1 + target_session_length

# Linear increasing rewards towards the target length, exponentially decreasing for everything higher than target length
def punish(tensor, target_session_length):
    return scale(tensor*F.threshold(tensor-target_session_length+1,1,1),target_session_length)

def punish2(tensor, target_session_length):
    return tensor*-F.threshold(tensor-target_session_length+1,1,-1)

# Positive linearly increasing upto target session length, negative linearly decreasing for everything larger than session length
def linear_punish(tensor, target_session_length, scale=1):
    # lin = lin_scale(tensor, target_session_length)
    thr = F.threshold(-F.threshold(tensor, target_session_length, -1), 0, -1*scale)
    add = (-F.threshold(-F.threshold(tensor, target_session_length, -1), 0, 0)+1) * target_session_length
    return tensor * thr + add

def ignore(tensor, target_session_length, scale=1):
    return ((tensor+1) / F.threshold(tensor+1, target_session_length+1, 1)) -1

def uniform_punish(tensor, target_session_length, scale=1):
    return ignore(tensor + scale, target_session_length + scale, scale) - scale

def do_nothing(tensor, target_session_length, scale=1):
    return tensor

def clip(tensor, target_session_length, scale=1):
    return ignore(tensor-target_session_length, 0, scale) + target_session_length
