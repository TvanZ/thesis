import random
import numpy as np
import math

# Define the user models

class UserModelSimple:
    """A class that functions as a user model.

    Parameters:
    - params: dict with letters a through g
        - a: the amount of previous steps to consider when not getting a reward
        - b: the negative impact of not getting a reward for consecutive turns
        - c: the negative impact of getting a reward from a certain bucket
        - d: the threshold for when a reward is considered large in comparison to the average
        - e: the postive impact of a large reward
        - f: the amount of previous steps to consider when getting a reward
        - g: the positive impact of getting a reward
        - h: the scaling factor for rewards larger than the average
    - bin_size: float indicating the size of the buckets. Default is 0.5
    - bin_edge: float indicating the value of the highest bin-edge. Default is 20
    Important: bin_edge / bin_size should be an integer, otherwise actual bin size will not correspond to bin_size
    """
    def __init__(self, params=None, bin_size=0.5, bin_edge=20, complete_session=False):
        if params == None:
            self.a = 5
            self.b = 0.06
            self.c = 0.1
            self.d = 1
            self.e = 0.01
            self.f = 5
            self.g = 0.05
            self.h = 1.5
        else:
            self.a = params['a']
            self.b = params['b']
            self.c = params['c']
            self.d = params['d']
            self.e = params['e']
            self.f = params['f']
            self.g = params['g']
            self.h = params['h']

        self.bins = int(bin_edge/bin_size)
        self.bin_size = bin_size
        self.bin_edge = bin_edge

        self.session_rewards = None
        self.session_boredom = None
        self.session_actions = None

        self.running_average = None
        self.previous_rewards = []
        self.max_reward_history = 200

        self.budget = 10
        self.tier = 1

        self.complete_session = complete_session

    def play(self, stimuli):
        response = None
        if len(stimuli.shape) == 1:
            response = self.play_session(stimuli)
        else:
            response = self.play_multiple_sessions(stimuli)
        return response

    def play_multiple_sessions(self, sessions):
        actions = []
        for session in sessions:
            actions.append(self.play_session(session))
        return np.array(actions)

    def play_session(self, rewards):
        # Beginning of session: store the rewards and create a list for the boredom values
        self.session_rewards = rewards
        self.session_boredom = [0. for i in range(len(rewards))]
        self.session_actions = [0. for i in range(len(rewards))]

        stopped_playing = False

        boredom = 0
        for i in range(len(rewards)):
            self.session_boredom[i] = boredom = self._calculate_boredom(i, boredom)
            user_action = random.choices(population=[0.,1.],weights=[min(1,boredom),1-min(1,boredom)])[0]
            self.previous_rewards.append(rewards[i].item())
            if user_action == 0.:
                stopped_playing = True
                if not self.complete_session:
                    break
            if not stopped_playing:
                self.session_actions[i] = user_action

        # update the running average of rewards
        received_rewards = [r for r in self.session_rewards[:i+1] if r > 0 ]
        if len(received_rewards) > 0:
            avg_received_reward = sum(received_rewards)/len(received_rewards)
            if self.running_average == None:
                self.running_average = avg_received_reward
            else:
                self.running_average += 0.1 * (avg_received_reward - self.running_average)

        # update the stored previous rewards to be smaller than the maximum size
        self.previous_rewards = self.previous_rewards[-self.max_reward_history:]

        return self.session_actions

    def _calculate_boredom(self, i, prev_boredom):
        b = prev_boredom
        current_reward = float(self.session_rewards[i].item())

        # Calculate the boredom increase from not getting rewards
        prev_rewards = list(self.session_rewards[max(0,i-self.a+1):i+1])
        b+= (prev_rewards.count(0) * self.b * (current_reward == 0))

        # Calculate the boredom increase from getting a reward of a predictable magnitude
        reward_density = 0
        if i > 0:
            (density, _) = np.histogram(self.previous_rewards, self.bins, range=(0,self.bin_edge), density=True)
            density *= self.bin_size
            (current_bin, _) = np.histogram(self.session_rewards[i], self.bins, range=(0,self.bin_edge))
            try:
                reward_density= density[list(current_bin).index(1)]
            except:
                reward_density= density[-1]
            b+= (reward_density * self.c * (current_reward > 0))

        # Calculate the boredeom decrease from getting a large reward
        avg_reward = i+1.
        if i > 0:
            previous_rewards = self.session_rewards[np.where(self.session_rewards[:i] > 0)]
            if self.running_average == None:
                if previous_rewards.size > 0:
                    avg_reward = np.mean(previous_rewards)
                else:
                    avg_reward = 1
            else:
                avg_reward = self.running_average
            if (current_reward / avg_reward - self.d) > 0:
                b-= ((current_reward / avg_reward - self.d)**self.h * ((current_reward / avg_reward - self.d) > 0) * self.e)

        # Calculate the boredeom decrease from getting a reward in general
        prev_rewards = list(self.session_rewards[max(0,i-self.f+1):i+1])
        b-= (prev_rewards.count(0) * self.g * (current_reward > 0) * (1-reward_density))

        return b



class UserModelAdvanced:
    """A class that functions as a user model.

    Parameters:
    - params: dict with letters a through g
        - a: the amount of previous steps to consider when not getting a reward
        - b: the negative impact of not getting a reward for consecutive turns
        - c: the negative impact of getting a reward from a certain bucket
        - d: the threshold for when a reward is considered large in comparison to the average
        - e: the postive impact of a large reward
        - f: the amount of previous steps to consider when getting a reward
        - g: the positive impact of getting a reward
        - h: the scaling factor for rewards larger than the average
    - bin_size: float indicating the size of the buckets. Default is 0.5
    - bin_edge: float indicating the value of the highest bin-edge. Default is 20
    Important: bin_edge / bin_size should be an integer, otherwise actual bin size will not correspond to bin_size
    """
    def __init__(self, params=None, bin_size=0.5, bin_edge=20, probs=False):
        if params == None:
            self.a = 5
            self.b = 0.06
            self.c = 0.1
            self.d = 1
            self.e = 0.01
            self.f = 5
            self.g = 0.05
            self.h = 1.5
        else:
            self.a = params['a']
            self.b = params['b']
            self.c = params['c']
            self.d = params['d']
            self.e = params['e']
            self.f = params['f']
            self.g = params['g']
            self.h = params['h']

        self.bins = int(bin_edge/bin_size)
        self.bin_size = bin_size
        self.bin_edge = bin_edge

        self.session_rewards = None
        self.session_boredom = None
        self.session_actions = None

        self.running_average = None
        self.previous_rewards = []
        self.max_reward_history = 200

        self.budget = 10
        self.tier = 1
        self.tier_thresholds = [10 * 2**x for x in range(11)]
        self.churned = False
        self.finished = False

        self.sessions_before_churn = 0
        self.alpha = 1.4
        self.coefficient = 9
        self._set_sessions_before_churn()

        self.probs = probs

        self.played_sessions = 0

    def play(self, stimuli):
        response = None
        if len(stimuli.shape) == 2:
         response = self.play_session(stimuli)
        else:
         response = self.play_multiple_sessions(stimuli)
        return response

    def play_multiple_sessions(self, sessions):
        actions = []
        tiers = []
        for session in sessions:
            a,t = self.play_session(session)
            actions.append(a)
            tiers.append(t)
        return np.array(actions),np.array(tiers),[len(actions)]

    def _upgrade_tier(self):
        if self.tier < 11 and self.budget >= self.tier_thresholds[self.tier]:
            self.tier += 1
            self._upgrade_tier()
            self._set_sessions_before_churn()
        if self.tier == 11:
            self.finished = True

    def _set_sessions_before_churn(self):
        r = 0
        while r == 0.:
            r = random.random()
        self.sessions_before_churn = math.floor(-(self.coefficient * self.tier**self.alpha) * math.log(r))

    def play_session(self, rewards):
        # Beginning of session: store the rewards and create a list for the boredom values
        self.session_rewards = rewards[self.tier-1]
        self.session_boredom = [0. for i in range(len(self.session_rewards))]
        self.session_actions = [0. for i in range(len(self.session_rewards))]

        stopped_playing = False

        boredom = 0
        for i in range(len(self.session_rewards)):
            self.budget += self.session_rewards[i]
            if (self.budget - self.tier) <= 0:
                self.churned = True
                break
            self.session_boredom[i] = boredom = self._calculate_boredom(i, boredom)
            user_action = random.choices(population=[0.,1.],cum_weights=[max(min(1.,boredom),0.),1])[0]
            self.previous_rewards.append(self.session_rewards[i].item())
            if user_action == 0.:
                stopped_playing = True
                if not self.probs:
                    break
            if not stopped_playing:
                self.budget -= self.tier
                self.session_actions[i] = user_action

        # Update the running average of rewards
        received_rewards = [r for r in self.session_rewards[:i+1] if r > 0 ]
        if len(received_rewards) > 0:
            avg_received_reward = sum(received_rewards)/len(received_rewards)
            if self.running_average == None:
                self.running_average = avg_received_reward
            else:
                self.running_average += 0.1 * (avg_received_reward - self.running_average)

        # Update the stored previous rewards to be smaller than the maximum size
        self.previous_rewards = self.previous_rewards[-self.max_reward_history:]

        # Reduce the number of sessions left before churning
        self.sessions_before_churn -= 1

        # Increment the number of sessions played
        self.played_sessions += 1

        # Create one hot vector for the tier this session was played at
        tier_vector = [1 if x+1 == self.tier else 0 for x in range(10)]

        # Check if the player should upgrade to the next tier
        self._upgrade_tier()

        if self.sessions_before_churn <= 0:
            self.churned = True

        probs_after_stimulus = 1-np.maximum(np.minimum(self.session_boredom, 1),0)
        probs = []
        for i,p in enumerate(probs_after_stimulus):
            probs.append(p*probs[-1] if len(probs)>0 else p)

        return self.session_actions if not self.probs else probs,tier_vector

    def _calculate_boredom(self, i, prev_boredom):
        b = prev_boredom
        current_reward = float(self.session_rewards[i].item())

        # Calculate the boredom increase from not getting rewards
        prev_rewards = list(self.session_rewards[max(0,i-self.a+1):i+1])
        b+= (prev_rewards.count(0) * self.b * (current_reward == 0))

        # Calculate the boredom increase from getting a reward of a predictable magnitude
        reward_density = 0
        if i > 0:
            (density, _) = np.histogram(self.previous_rewards, self.bins, range=(0,self.bin_edge*self.tier**2), density=True)
            density *= self.bin_size
            (current_bin, _) = np.histogram(self.session_rewards[i], self.bins, range=(0,self.bin_edge*self.tier**2))
            try:
                reward_density= density[list(current_bin).index(1)]
            except:
                reward_density= density[-1]
            b+= (reward_density * self.c * (current_reward > 0))

        # Calculate the boredeom decrease from getting a large reward
        avg_reward = i+1.
        if i > 0:
            previous_rewards = self.session_rewards[np.where(self.session_rewards[:i] > 0)]
            if self.running_average == None:
                if previous_rewards.size > 0:
                    avg_reward = np.mean(previous_rewards)
                else:
                    avg_reward = 1
            else:
                avg_reward = self.running_average
            if (current_reward / avg_reward - self.d) > 0:
                b-= ((current_reward / avg_reward - self.d)**self.h * ((current_reward / avg_reward - self.d) > 0) * self.e)

        # Calculate the boredeom decrease from getting a reward in general
        prev_rewards = list(self.session_rewards[max(0,i-self.f+1):i+1])
        b-= (prev_rewards.count(0) * self.g * (current_reward > 0) * (1-reward_density))

        return b

class Cohort:
    def __init__(self, player_model, nr_of_players=1000, replace=True, player_parameters={}, probs=False):
        assert player_model != None
        self.player_model = player_model
        self.nr_of_players = nr_of_players
        self.replace = replace
        self.player_parameters = player_parameters
        self.probs = probs

        self.active_players = [self.player_model(self.player_parameters, probs=self.probs) for x in range(nr_of_players)]
        self.churned_players = []
        self.finished_players = []

        self.players_per_tier_per_session = []

        self.played_sessions = 0

    def play(self, stimuli):
        cohort_response = [m.play(s) for m,s in zip(self.active_players,stimuli)]
        cohort_actions, cohort_tiers = [[a for a,t in cohort_response],[t for a,t in cohort_response]]

        self.churned_players.append([m for m in self.active_players if m.churned == True])
        self.finished_players.append([m for m in self.active_players if m.finished == True])
        self.active_players = [m for m in self.active_players if (m.churned == False and m.finished == False)]
        self.players_per_tier_per_session.append(
            np.histogram([m.tier -1 for m in self.active_players], 10, range=(0,10))[0].tolist()
        )

        self.played_sessions += 1

        if self.replace and len(self.active_players) < self.nr_of_players:
            new_players = [self.player_model(self.player_parameters)
            for x in range(
            self.nr_of_players - len(self.active_players)
            )]
            self.active_players += new_players

        # print(cohort_actions)
        # print(cohort_tiers)
        return cohort_actions, cohort_tiers

class PlayerBase:
    def __init__(self, player_model, player_parameters, nr_of_cohorts=1, cohort_interval=5, cohort_endpoint=50, players_per_cohort=1000, replace=True, probs=False):
        self.player_model = player_model
        self.nr_of_cohorts = nr_of_cohorts
        self.cohort_interval = cohort_interval
        self.cohort_endpoint = cohort_endpoint
        self.last_cohort_introduction = (nr_of_cohorts - 1) * cohort_interval
        self.players_per_cohort = players_per_cohort
        self.replace = replace
        self.player_parameters = player_parameters
        self.probs = probs

        self.cohorts = [Cohort(self.player_model, self.players_per_cohort, self.replace, self.player_parameters, probs=self.probs)]
        self.retired_cohorts = []

        self.session = 0

    def play(self,stimuli):
        assert len(stimuli) == self.players_per_cohort

        all_player_actions = []
        all_player_tiers = []
        players_per_cohort = []
        for cohort in self.cohorts:
            cohort_actions, cohort_tiers = cohort.play(stimuli)
            players_per_cohort.append(len(cohort_actions))
            all_player_actions += cohort_actions
            all_player_tiers += cohort_tiers

        self.session += 1

        if not self.nr_of_cohorts == 1 and len(self.cohorts) > 0:
            if self.cohorts[0].played_sessions == self.cohort_endpoint:
                self.retired_cohorts.append(self.cohorts[0])
                self.cohorts = self.cohorts[1:]

            if self.session % self.cohort_interval == 0 and self.session <= self.last_cohort_introduction:
                self.cohorts.append(Cohort(self.player_model, self.players_per_cohort, self.replace, self.player_parameters, probs=self.probs))

        return np.array(all_player_actions), np.array(all_player_tiers), players_per_cohort
