#!/usr/bin/env python

import os,sys
import random
import math
import json
import numpy as np
import torch
import pickle
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.distributions.bernoulli import Bernoulli
from torch.distributions.gamma import Gamma
import itertools
import pandas
import time
import pickle
import csv

from pathlib import Path
base_path = Path(__file__).parent


torch.cuda.empty_cache()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


import usermodels
import actorcritic
import rewardfunctions


### EXECUTE GRID SEARCH OVER CONFIGURATION PARAMETERS ###

def grid_search(parameter_ranges, outfile):
    parameter_combinations = itertools.product(*parameter_ranges.values())
    results = pandas.DataFrame(index=range(4608), columns=['parameters','full_sessions_init','avg_sessions_init','std_sessions_init','full_sessions','avg_sessions','std_sessions'])

    for x,combination in enumerate(parameter_combinations):
        start_time = time.time()
        params = {}
        for i,key in enumerate(parameter_ranges.keys()):
            params[key] = combination[i]
            if key == 'a':
                params['f'] = combination[i]
        um = UserModelSimple(params)
        a = Actor()
        c = Critic()
        optim_a = optim.Adam(a.parameters(), lr=0.1)
        optim_c = optim.Adam(c.parameters(), lr=0.01)
        a.to(device)
        c.to(device)

        for j in range(5):
            session_lengths, rewards = train(a, c, um, optim_a, optim_c, 100)
            if j == 0:
                initial_session_lengths = session_lengths

        nr_of_full_sessions_initial = np.histogram(initial_session_lengths, 50, range=(0,50))[0][-1]
        avg_session_length_initial = round(np.mean(initial_session_lengths).item(),2)
        std_session_length_initial = round(np.std(initial_session_lengths).item(),2)
        nr_of_full_sessions = np.histogram(session_lengths, 50, range=(0,50))[0][-1]
        avg_session_length = round(np.mean(session_lengths).item(),2)
        std_session_length = round(np.std(session_lengths).item(),2)



        results.loc[x] = str(combination),nr_of_full_sessions_initial,avg_session_length_initial,std_session_length_initial,nr_of_full_sessions,avg_session_length,std_session_length
        end_time = time.time()
        print(x, round(end_time-start_time,2),avg_session_length_initial,avg_session_length)

    results.to_csv(outfile)


# - a: the amount of previous steps to consider when not getting a reward
# - b: the negative impact of not getting a reward for consecutive turns
# - c: the negative impact of getting a reward from a certain bucket
# - d: the threshold for when a reward is considered large in comparison to the average
# - e: the postive impact of a large reward
# - f: the amount of previous steps to consider when getting a reward
# - g: the positive impact of getting a reward
# - h: the scaling factor for rewards larger than the average

parameter_ranges = {
    'a': [x for x in range(5,11)],
    'b': [0.01,0.03,0.05,0.07],
    'c': [0.01,0.03,0.05,0.07],
    'd': [1],
    'e': [0.01,0.03,0.05,0.07],

    'g': [0.01,0.03,0.05,0.07],
    'h': [1, 1.5, 2]
}
outfile = './Outputs/grid_search.csv'

grid_search(parameter_ranges,outfile)

# Analyse the grid search
df = pandas.read_csv(outfile)
df['grow_factor'] = df['avg_sessions']/df['avg_sessions_init']
filtered_df = df[(df['avg_sessions_init'] < 10) & (df['full_sessions_init'] < 2)]

# Select the parameter combinations to evaluate further
selected_params = {}
selected_params['most_full_sessions'] = eval(filtered_df.nlargest(1, 'full_sessions').iloc[0]['parameters'])
selected_params['smallest_avg'] = eval(filtered_df.nsmallest(1, 'avg_sessions_init').iloc[0]['parameters'])
selected_params['largest_grow'] = eval(filtered_df.nlargest(1, 'grow_factor').iloc[0]['parameters'])
selected_params['smallest_grow'] = eval(filtered_df.nsmallest(1, 'grow_factor').iloc[0]['parameters'])
selected_params['smallest_sd'] = eval(filtered_df.nsmallest(1, 'std_sessions_init').iloc[0]['parameters'])
selected_params['largest_sd'] = eval(filtered_df.nlargest(1, 'std_sessions_init').iloc[0]['parameters'])

for nr,k in enumerate(selected_params):
    value = selected_params[k]
    params = {}
    for i,key in enumerate(parameter_ranges.keys()):
        params[key] = value[i]
        if key == 'a':
            params['f'] = value[i]

    um = usermodels.UserModelSimple(params=params)
    a = actorcritic.Actor()
    c = actorcritic.Critic()
    a.to(device)
    c.to(device)
    optim_a = optim.Adam(a.parameters(), lr=0.1)
    optim_c = optim.Adam(c.parameters(), lr=0.01)

    file_path = (base_path / f"./Outputs/parameter_analysis_{value}.csv").resolve()
    actorcritic.train(a, c, um, optim_a, optim_c, iterations=500, save_to_file=file_path)




### RUN EXPERIMENTS ###

reward_functions = {
    'ignore': rewardfunctions.ignore,
    'linear_punish': rewardfunctions.linear_punish,
    'uniform_punish': rewardfunctions.uniform_punish,
    'clip': rewardfunctions.clip,
    'regular': rewardfunctions.do_nothing
}

tau = 33.33

for nr,k in enumerate(selected_params):
    print(k)
    value = selected_params[k]
    params = {}
    for i,key in enumerate(parameter_ranges.keys()):
        params[key] = value[i]
        if key == 'a':
            params['f'] = value[i]

    file_path_actor_p = (base_path / f"./Outputs/{k}_actor_p.csv").resolve()
    with open(file_path_actor_p, 'w') as f:
        f.write('func,1,2,3,4,5,6,7,8,9,10\n')
    file_path_actor_conc = (base_path / f"./Outputs/{k}_actor_conc.csv").resolve()
    with open(file_path_actor_conc, 'w') as f:
        f.write('func,1,2,3,4,5,6,7,8,9,10\n')


    for e,func in enumerate(reward_functions):
        if not (k == 'largest_sd' or func == 'regular'):
            continue
        print(func)
        pl = usermodels.PlayerBase(usermodels.UserModelAdvanced,params, replace=False, nr_of_cohorts=100, )
        a = actorcritic.Actor()
        c = actorcritic.Critic()
        a.to(device)
        c.to(device)
        optim_a = optim.Adam(a.parameters(), lr=0.01)
        optim_c = optim.Adam(c.parameters(), lr=0.01)

        file_path = (base_path / f"./Outputs/{k}_{func}_{tau}.csv").resolve()
        file_path_pkl = (base_path / f"./Outputs/{k}_{func}_{tau}.pkl").resolve()

        actorcritic.train(a, c, pl, optim_a, optim_c, iterations=595, save_to_file=file_path, rewardfunction=reward_functions[func], tau=tau)
        obj = {'pl': pl, 'a': a, 'c': c}

        with open(file_path_pkl, 'wb') as f:
            pickle.dump(obj, f)

        file_path_session_lengths = (base_path / f"./Outputs/{k}_{func}_{tau}_session_lengths.csv").resolve()
        file_path_churn_rates = (base_path / f"./Outputs/{k}_{func}_{tau}_churn_rates.csv").resolve()


        # Extract the session lengths per tier
        df = pandas.read_csv(file_path, quotechar="'")
        means = df.loc[:,'mean_per_tier']

        with open(file_path_session_lengths, 'w') as f:
            f.write('iteration,1,2,3,4,5,6,7,8,9,10\n')

        for i,row in enumerate(means):
            if not type(row) == float:
                clean_string = row.replace('[','').replace(']','')
                with open(file_path_session_lengths, 'a') as f:
                    f.write(str(i)+','+clean_string+'\n')

        with open(file_path_pkl, 'rb') as f:
            obj=pickle.load(f)
        pl = obj['pl']
        cohorts = pl.retired_cohorts

        # Extract the churn rates per tier
        with open(file_path_churn_rates, 'w') as f:
            f.write('iteration,1,2,3,4,5,6,7,8,9,10,11\n')

        for i,cohort in enumerate(cohorts):
            iteration = i * 5
            all_churned_players = [p for it in cohort.churned_players for p in it]
            players_final_level = [p.tier for p in all_churned_players]
            churned_amount_per_tier = np.histogram(players_final_level, bins=10, range=(0,50))[0].tolist()
            remaining_players = 1000
            churn_rate = []
            for c in churned_amount_per_tier:
                churn_rate.append(c/remaining_players)
                remaining_players -= c
            churn_rate.append(remaining_players/1000)
            churn_str = ', '.join(str(e) if e > 0 else 'nan' for e in churn_rate)
            with open(file_path_churn_rates, 'a') as f:
                f.write(str(iteration) + ','+ churn_str +'\n')

        # Extract the parameter values of the actor
        p = df.iloc[500]['p']
        conc = df.iloc[500]['concentration']
        with open(file_path_actor_p, 'a') as f:
            f.write(func+','+p+'\n')
        with open(file_path_actor_conc, 'a') as f:
            f.write(func+','+conc+'\n')



### EVALUATE THE LEARNING OF THE CRITIC ###

for nr,k in enumerate(selected_params):
    if not nr in [5] :
        continue
    print(k)
    value = selected_params[k]
    params = {}
    for i,key in enumerate(parameter_ranges.keys()):
        params[key] = value[i]
        if key == 'a':
            params['f'] = value[i]
    # print(params)
    for e,func in enumerate(reward_functions):
        # if not e in [0]:
        #     continue
        print(func)
        file_path = (base_path / f"./Outputs/critic_eval_probs_{k}_{func}_{tau}.csv").resolve()
        file_path_pkl = (base_path / f"./Outputs/{k}_{func}_{tau}.pkl").resolve()

        with open(file_path_pkl, 'rb') as f:
            obj = pickle.load(f)

        pl = usermodels.PlayerBase(usermodels.UserModelAdvanced,params, replace=False, nr_of_cohorts=50, probs=True)
        a = obj['a']
        c = actorcritic.Critic()
        a.to(device)
        c.to(device)
        optim_a = optim.Adam(a.parameters(), lr=0.01)
        optim_c = optim.Adam(c.parameters(), lr=0.01)

        actorcritic.eval_critic(a, c, pl, optim_a, optim_c, iterations=295, save_to_file=file_path, rewardfunction=reward_functions[func], tau=tau)
        obj = {'pl': pl, 'a': a, 'c': c}

        file_path_pkl_2 = (base_path / f"./Outputs/critic_eval_probs_{k}_{func}_{tau}.pkl").resolve()
        with open(file_path_pkl_2, 'wb') as f:
            pickle.dump(obj, f)

        df = pandas.read_csv(file_path, quotechar="'")
        residuals = df.iloc[250]['residuals']
        predictions = df.iloc[250]['predictions']
        res_list = residuals.split(',')
        pred_list = predictions.split(',')

        file_path_res = (base_path / f"./Outputs/critic_eval_probs_res_{k}_{func}_{tau}.csv").resolve()
        with open(file_path_res, 'w') as f:
            f.write('pred,res\n')
            for res,pred in zip(res_list, pred_list):
                f.write(pred+','+res+'\n')

